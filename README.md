
Implementation of the following CPU scheduling algorithms:

* First-Come-First-Served
* Round-Robin
* Short-Job-First
* Shortest-Remaining-Time-First

# Usage

```
$ raku bin/fcfs.raku --help
Usage:
  bin/fcfs.raku
  bin/fcfs.raku --table <file>
  bin/fcfs.raku --csv <file>
```

where `<file>` is a CSV file in the following format:

```
Process,Arrival Time,Service Time,Priority
P1,0,6,4
P2,2,6,3
P3,4,4,2
P4,5,5,2
P5,8,3,1
```

If no command arguments are provided, input is taken from standard input in the
same format as above.

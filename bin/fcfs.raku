
enum State<READY EXECUTING TERMINATED NONE>;

class Process {
    # constructor.
    has Str $.pid      is required("we must know process's PID");
    has Int $.arrival  is required("we must know process's arrival time");
    has Int $.service  is required("we must know process's service time");
    has Int $.priority is required("we must know process's priority");

    # states alterable after construction.
    has Int    $.remaining      is rw = $!service;
    has State  $.state          is rw = NONE;
    has Int    $.start          is rw;
    has Int    $.finish         is rw;
    has Bool:D $.is-scheduled   is rw = False;

    method waiting {
        $!start.defined.so 
        ?? $!start - $!arrival
        !! Nil
    }

    method turnaround {
        self.waiting ~~ Nil
        ?? Nil
        !! $!service + self.waiting
    }

    method normed-turnaround {
        self.turnaround ~~ Nil
        ?? Nil
        !! (self.turnaround / $!service).round(0.01);
    }

    method reset(--> Nil) {
        $!remaining    = $!service;
        $!state        = NONE;
        $!start        = Int;
        $!finish       = Int;
        $!is-scheduled = False;
    }

    method gist {
        sprintf "PID: %-5s AT: %-5d ST: %-5d", $!pid, $!arrival, $!service;
    }
}

class FCFS {
    has Process:D @.processes is required('processes are needed to run simulation');

    submethod TWEAK {
        self!fcfs
    }

    #
    # Interface
    #
    
    multi method mean(Bool :$waiting!) {
        (@!processes».waiting.sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$turnaround!) {
        (@!processes».turnaround.sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$normed-turnaround!) {
        (@!processes».normed-turnaround.sum / @!processes.elems).round(0.01)
    }

    method csv {
        my @data = gather for @!processes {
            take (.pid, .arrival, .service, .priority, .waiting, .turnaround, .normed-turnaround)
        };

        @data.push: ('means', '', '', '', self.mean(:waiting), self.mean(:turnaround), self.mean(:normed-turnaround));

        return @data;
    }

    multi method gist {
        self.gist(:table)
    }

    multi method gist(Bool :$csv! --> Str:D) {
        self.csv.map(*.join(',')).join("\n")
    }

    multi method gist(Bool :$table!) {
        my @headers = 'PID', 'Arrival', 'Service', 'Priority', 'Waiting', 'Turnaround', 'N. Turnaround';
        my $fmt = "%-10s %-10s %-10s %-10s %-10s %-10s %-10s";

        my @output = gather for self.csv {
            take sprintf $fmt, |$_;
        }

        @output.unshift(sprintf $fmt, |@headers);

        return @output.join("\n");
    }

    #
    # Internals
    # 

    method !fcfs {
        for @!processes.keys -> \i {
            if i == 0 {
                @!processes[i].start  = @!processes[i].arrival;
                @!processes[i].finish = @!processes[i].service;
            }
            else {
                @!processes[i].start  = @!processes[i-1].finish;
                @!processes[i].finish = @!processes[i].start + @!processes[i].service;
            }
        }
    }
}

sub get-processes(IO::Handle:D $fh --> Array:D) {
    # read file and create processes (assume csv file has a header which is ignored.)
    (gather for $fh.lines[1..*].grep(*.chars > 0) -> $line {
        my ($pid, $arrival, $service, $priority) = $line.split(',', :skip-empty);
        $arrival  .= Int;
        $service  .= Int;
        $priority .= Int;
        take Process.new: :$pid, :$arrival, :$service, :$priority;
    }).Array
}

multi MAIN {
    my $fh = $*IN;
    my Process:D @processes = get-processes($fh);
    say FCFS.new(:@processes).gist(:table);
}

multi MAIN(
    Str $file,
    Bool:D :$table!,
) {
    my $fh = $file.IO.open;
    my Process:D @processes = get-processes($fh);
    say FCFS.new(:@processes).gist(:table);
}

multi MAIN(
    Str $file,
    Bool:D :$csv!,
) {
    my $fh = $file.IO.open;
    my Process @processes = get-processes($fh);
    say FCFS.new(:@processes).gist(:csv);
}

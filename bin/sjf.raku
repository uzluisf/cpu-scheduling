
enum State<READY EXECUTING TERMINATED NONE>;

class Process {
    # constructor.
    has Str $.pid      is required("we must know process's PID");
    has Int $.arrival  is required("we must know process's arrival time");
    has Int $.service  is required("we must know process's service time");
    has Int $.priority is required("we must know process's priority");

    # states alterable after construction.
    has Int    $.remaining      is rw = $!service;
    has State  $.state          is rw = NONE;
    has Int    $.start          is rw;
    has Int    $.finish         is rw;
    has Bool:D $.is-scheduled   is rw = False;

    method waiting {
        ($!arrival, $!start)>>.defined.all.so
        ?? $!start - $!arrival
        !! Nil
    }

    method turnaround {
        self.waiting ~~ Nil
        ?? Nil
        !! $!service + self.waiting
    }

    method normed-turnaround {
        self.turnaround ~~ Nil
        ?? Nil
        !! (self.turnaround / $!service).round(0.01);
    }

    method reset(--> Nil) {
        $!remaining    = $!service;
        $!state        = NONE;
        $!start        = Int;
        $!finish       = Int;
        $!is-scheduled = False;
    }

    method gist {
        sprintf "PID: %-5s AT: %-5d ST: %-5d", $!pid, $!arrival, $!service;
    }
}

class SJF {
    has Process @.processes;

    submethod TWEAK {
        self!sfj
    }

    #
    # Interface
    #
    
    multi method mean(Bool :$waiting!) {
        (@!processes».waiting.sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$turnaround!) {
        (@!processes».turnaround.sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$normed-turnaround!) {
        (@!processes».normed-turnaround.sum / @!processes.elems).round(0.01)
    }

    method csv {
        my @data = gather for @!processes {
            take (.pid, .arrival, .service, .priority, .waiting, .turnaround, .normed-turnaround)
        };

        @data.push: ('means', '', '', '', self.mean(:waiting), self.mean(:turnaround), self.mean(:normed-turnaround));

        return @data;
    }

    multi method gist {
        self.gist(:table)
    }

    multi method gist(Bool :$csv! --> Str:D) {
        self.csv.map(*.join(',')).join("\n")
    }

    multi method gist(Bool :$table!) {
        my @headers = 'PID', 'Arrival', 'Service', 'Priority', 'Waiting', 'Turnaround', 'N. Turnaround';
        my $fmt = "%-10s %-10s %-10s %-10s %-10s %-10s %-10s";

        my @output = gather for self.csv {
            take sprintf $fmt, |$_;
        }

        @output.unshift(sprintf $fmt, |@headers);

        return @output.join("\n");
    }

    #
    # Internals
    #

    # perform the Short-Job-First (SJF) CPU scheduling algorithm.
    method !sfj {
        my @queue; # ready queue.
        my $job;   # current job being run in CPU.
        my $start; # next job's start time.
        my $end;   # next job's finish time.

        # run first process on CPU.
        @queue[0]         = @!processes[0];
        $job              = @queue.shift;
        $start            = $job.arrival;
        $end              = $job.service;
        $job.start        = $start;
        $job.finish       = $job.start + $job.service;
        $job.is-scheduled = True;

        loop {
            # queue up unscheduled processes that arrives
            # while shortest job in ready queue is running.
            for @!processes -> $process {
                next if $process.is-scheduled;

                if $start ≤ $process.arrival ≤ $end {
                    @queue.push: $process
                }
            }

            # remove current shortest job from ready queue. 
            remove-process $job, @queue;

            # continue simulation is queue isn't empty. Otherwise,
            # finish simulation.
            if @queue {
                # grab job shortest job from ready queue.
                $job = @queue.sort(*.service).head;

                # set job's start and finish times.
                $job.start        = $end;
                $job.finish       = $job.start + $job.service;
                $job.is-scheduled = True;

                $start = $job.start;
                $end   = $job.finish;
            }
            else {
                last
            }
        }
        
        # remove process from ready queue.
        sub remove-process($process, @queue) {
            my $index = 0;
            for @queue {
                if $_.pid eq $process.pid { last; }
                $index += 1;
            }

            @queue.splice($index, 1);
        }
    }
}

sub get-processes(IO::Handle:D $fh --> Array:D) {
    # read file and create processes (assume csv file has a header which is ignored.)
    (gather for $fh.lines[1..*].grep(*.chars > 0) -> $line {
        my ($pid, $arrival, $service, $priority) = $line.split(',', :skip-empty);
        $arrival  .= Int;
        $service  .= Int;
        $priority .= Int;
        take Process.new: :$pid, :$arrival, :$service, :$priority;
    }).Array
}

multi MAIN {
    my $fh = $*IN;
    my Process:D @processes = get-processes($fh);
    say SJF.new(:@processes).gist(:table);
}

multi MAIN(
    Str $file,
    Bool:D :$table!,
) {
    my $fh = $file.IO.open;
    my Process:D @processes = get-processes($fh);
    say SJF.new(:@processes).gist(:table);
}

multi MAIN(
    Str $file,
    Bool:D :$csv!,
) {
    my $fh = $file.IO.open;
    my Process @processes = get-processes($fh);
    say SJF.new(:@processes).gist(:csv);
}



enum State<READY EXECUTING TERMINATED NONE>;

class Process {
    # constructor.
    has Str $.pid      is required("we must know process's PID");
    has Int $.arrival  is required("we must know process's arrival time");
    has Int $.service  is required("we must know process's service time");
    has Int $.priority is required("we must know process's priority");

    # states alterable after construction.
    has Int    $.remaining      is rw = $!service;
    has State  $.state          is rw = NONE;
    has Int    $.start          is rw;
    has Int    $.finish         is rw;
    has Bool:D $.is-scheduled   is rw = False;

    method waiting {
        ($!finish, $!arrival, $!service)>>.defined.all.so
        ?? $!finish - $!arrival - $!service
        !! Nil
    }

    method turnaround {
        ($!finish, $!arrival)>>.defined.all.so
        ?? $!finish - $!arrival
        !! Nil
    }

    method normed-turnaround {
        self.turnaround ~~ Nil
        ?? Nil
        !! (self.turnaround / $!service).round(0.01);
    }

    method reset(--> Nil) {
        $!remaining    = $!service;
        $!state        = NONE;
        $!start        = Int;
        $!finish       = Int;
        $!is-scheduled = False;
    }
}

class RoundRobin {
    has Process:D @.processes is required("processes are needed to run simulation");
    has Int $.quantum         is required("processes need a time slice");

    method TWEAK {
        self!round-robin($!quantum);
    }

    #
    # INTERFACE
    #

    method simulate(Int $quantum) {
        # reset processes to original state.
        @!processes».reset;
        # simulate round-robin with possibly new quantum.
        self!round-robin($quantum // $!quantum);
    }

    multi method mean(Bool :$waiting!) {
        (@!processes.map(*.waiting).sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$turnaround!) {
        (@!processes.map(*.turnaround).sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$normed-turnaround!) {
        (@!processes.map(*.normed-turnaround).sum / @!processes.elems).round(0.01)
    }

    method csv {
        my @data = gather for @!processes {
            take (.pid, .arrival, .service, .priority, .waiting, .turnaround, .normed-turnaround)
        };

        @data.push: ('means', '', '', '', self.mean(:waiting), self.mean(:turnaround), self.mean(:normed-turnaround));

        return @data;
    }

    multi method gist {
        self.gist(:table)
    }

    multi method gist(Bool :$csv! --> Str:D) {
        self.csv.map(*.join(',')).join("\n")
    }

    multi method gist(Bool :$table!) {
        my @headers = 'PID', 'Arrival', 'Service', 'Priority', 'Waiting', 'Turnaround', 'N. Turnaround';
        my $fmt = "%-10s %-10s %-10s %-10s %-10s %-10s %-10s";

        my @output = gather for self.csv {
            take sprintf $fmt, |$_;
        }

        @output.unshift(sprintf $fmt, |@headers);

        return @output.join("\n");
    }
       
    #
    # INTERNALS
    #

    # perform the round-robin scheduling algorithm with given processes and
    # quantum.
    method !round-robin(Int $quantum) {
        my @jobs = @!processes;
        my @queue;
        my Int:D $quantum-step  = 1;
        
        @queue[0]              := @jobs[0];
        @queue[0].start        = 0;
        @queue[0].state        = EXECUTING;
        @queue[0].is-scheduled = True;

        while @queue {
            my $head = @queue.shift;
            my $proc-slice = 1;

            loop {
                # exit loop if process's quantum expired.
                if $proc-slice > $quantum {
                    last;
                }
        
                $proc-slice     += 1;
                $head.remaining -= 1;
                $quantum-step   += 1;
        
                # if process has no remaining time left, exit loop.
                last if $head.remaining == 0;
                
                # search for processes that haven't been placed into the ready
                # queue yet and that are ready to be placed in it.
                for @jobs -> $job {
                    if !$job.is-scheduled && $job.arrival < $quantum-step {
                        $job.state        = READY;
                        $job.is-scheduled = True;
                        @queue.push($job);
                    }
                }
            }
        
            # place process back into ready queue if it still has remaining time. 
            if $head.remaining > 0 {
                $head.state = READY;
                @queue.push: $head;
            }
            # otherwise, mark it as terminated and set its finish time.
            else {
                $head.finish = $quantum-step - 1;
                $head.state  = TERMINATED;
            }
        
            # if ready queue isn't empty, mark next process to run. If
            # the next process has never run, set its start time.
            if @queue {
                @queue[0].state = EXECUTING;
        
                if !@queue[0].start.defined {
                    @queue[0].start = $quantum-step - 1;
                }
            }
        }
    }

}

sub get-processes(IO::Handle:D $fh --> Array:D) {
    # read file and create processes (assume csv file has a header which is ignored.)
    (gather for $fh.lines[1..*].grep(*.chars > 0) -> $line {
        my ($pid, $arrival, $service, $priority) = $line.split(',', :skip-empty);
        $arrival  .= Int;
        $service  .= Int;
        $priority .= Int;
        take Process.new: :$pid, :$arrival, :$service, :$priority;
    }).Array
}

multi MAIN {
    my $fh = $*IN;
    my Process:D @processes = get-processes($fh);
    say RoundRobin.new(:@processes, quantum => 3).gist(:table);
}

multi MAIN(
    Str $file,
    Int :$quantum = 3,
    Bool:D :$table!,
) {
    my $fh = $file.IO.open;
    my Process:D @processes = get-processes($fh);
    say RoundRobin.new(:@processes, :$quantum).gist(:table);
}

multi MAIN(
    Str $file,
    Int :$quantum = 3,
    Bool:D :$csv!,
) {
    my $fh = $file.IO.open;
    my Process @processes = get-processes($fh);
    say RoundRobin.new(:@processes, :$quantum).gist(:csv);
}

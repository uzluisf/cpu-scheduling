
enum State<READY EXECUTING TERMINATED NONE>;

class Process {
    # constructor.
    has Str $.pid      is required("we must know process's PID");
    has Int $.arrival  is required("we must know process's arrival time");
    has Int $.service  is required("we must know process's service time");
    has Int $.priority is required("we must know process's priority");

    # states alterable after construction.
    has Int    $.remaining      is rw = $!service;
    has State  $.state          is rw = NONE;
    has Int    $.start          is rw;
    has Int    $.finish         is rw;
    has Bool:D $.is-scheduled   is rw = False;

    method waiting {
        $!start.defined.so 
        ?? $!finish - $!arrival - $!service
        !! Nil
    }

    method turnaround {
        self.waiting ~~ Nil
        ?? Nil
        !! $!finish - $!arrival
    }

    method normed-turnaround {
        self.turnaround ~~ Nil
        ?? Nil
        !! (self.turnaround / $!service).round(0.01);
    }

    method reset(--> Nil) {
        $!remaining    = $!service;
        $!state        = NONE;
        $!start        = Int;
        $!finish       = Int;
        $!is-scheduled = False;
    }

    method gist {
        sprintf "PID: %-5s AT: %-5d VT: %-5d RT: %-5d",
            $!pid, $!arrival, $!service, $!remaining;
    }
}

class SRTF {
    has Process:D @.processes;

    submethod TWEAK {
        self!srtf;
    }

    #
    # Interface
    #

    multi method mean(Bool :$waiting!) {
        (@!processes».waiting.sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$turnaround!) {
        (@!processes».turnaround.sum / @!processes.elems).round(0.01)
    }

    multi method mean(Bool :$normed-turnaround!) {
        (@!processes».normed-turnaround.sum / @!processes.elems).round(0.01)
    }

    method csv {
        my @data = gather for @!processes {
            take (.pid, .arrival, .service, .priority, .waiting, .turnaround, .normed-turnaround)
        };

        @data.push: ('means', '', '', '', self.mean(:waiting), self.mean(:turnaround), self.mean(:normed-turnaround));

        return @data;
    }

    multi method gist {
        self.gist(:table)
    }

    multi method gist(Bool :$csv! --> Str:D) {
        self.csv.map(*.join(',')).join("\n")
    }

    multi method gist(Bool :$table!) {
        my @headers = 'PID', 'Arrival', 'Service', 'Priority', 'Waiting', 'Turnaround', 'N. Turnaround';
        my $fmt = "%-10s %-10s %-10s %-10s %-10s %-10s %-10s";

        my @output = gather for self.csv {
            take sprintf $fmt, |$_;
        }

        @output.unshift(sprintf $fmt, |@headers);

        return @output.join("\n");
    }

    #
    # Internals
    #

    method !srtf {
        my Process @queue = @!processes[0];
        @queue[0].start   = 0;
        my Int $time      = 0;
        loop {
            $time               += 1;
            @queue[0].remaining -= 1;

            with @!processes.first(*.arrival == $time) -> $proc {
                # preempt current running process.
                if $proc.remaining < @queue[0].remaining {
                    $proc.start   = $time;
                    my $preempted = @queue.shift;
                    @queue.unshift: $proc; 
                    @queue.push: $preempted;
                }
                # preempt newly arrived process.
                else {
                    @queue.push: $proc;
                }
            }

            # current running process's remaining time reaches zero.
            if @queue[0].remaining == 0 {
                # set its finish time and remove it from queue.
                @queue[0].finish = $time;
                @queue.shift;

                # set process-to-run-next's start time unless already set.
                if @queue && !@queue[0].start.defined {
                    @queue[0].start = $time;
                }
            }

            # queue's empty so stop simulation.
            last unless @queue;

            my $curr = @queue[0];
            my $i;

            # try to find process whose remaining time is
            # less than the current process's.
            for @queue.kv -> \k, \v {
                if v.remaining < $curr.remaining {
                    ($curr, $i) = (v, k)
                }
            }

            # if we find such process, place it at the queue
            # so it's ready to run.
            unless @queue[0].pid eq $curr.pid {
                my Process $torun = @queue.splice($i, 1)[0];
                $torun.start      = $time unless $torun.start.defined;
                @queue.unshift:     $torun;
            }
        }
    }
}

sub get-processes(IO::Handle:D $fh --> Array:D) {
    # read file and create processes (assume csv file has a header which is ignored.)
    (gather for $fh.lines[1..*].grep(*.chars > 0) -> $line {
        my ($pid, $arrival, $service, $priority) = $line.split(',', :skip-empty);
        $arrival  .= Int;
        $service  .= Int;
        $priority .= Int;
        take Process.new: :$pid, :$arrival, :$service, :$priority;
    }).Array
}

multi MAIN {
    my $fh = $*IN;
    my Process:D @processes = get-processes($fh);
    say SRTF.new(:@processes).gist(:table);
}

multi MAIN(
    Str $file,
    Bool:D :$table!,
) {
    my $fh = $file.IO.open;
    my Process:D @processes = get-processes($fh);
    say SRTF.new(:@processes).gist(:table);
}

multi MAIN(
    Str $file,
    Bool:D :$csv!,
) {
    my $fh = $file.IO.open;
    my Process @processes = get-processes($fh);
    say SRTF.new(:@processes).gist(:csv);
}

